---
title: Home
slug: "/"
hideTime: true
hideTitle: true
---


# Welcome to the Higher-Ed DevOps Conference!

Hello and welcome! On the afternoons (1-5 EDT) of June 2-4, the Higher-Ed DevOps Conference will be meeting to share training, best practices, and more. If you are faculty/staff of any InCommon institution, you are welcome to join us... for free!

Due to the on-going pandemic, this inaugural conference will be presented in a virtual-only format. The majority of session material will be pre-recorded (to minimize network availability issues), but live chat and Q&As will also be hosted.

This event is a satellite event of the [Cloud Forum](https://blogs.cornell.edu/cloudforum/).


## Register now!

Conference registration is now opened! If you are employed by a Higher-Ed institution in the US, you are welcome to attend! Simply click the below to go to a Google Form used for registration.

{{< register-button >}}

If you are interested in seeing what sessions we have lined up so far, be sure to check out the [session list](/agenda).


## The Planning Committee

Our planning committee includes representatives from the universities listed below. We are excited to get this conference going and hope it is a welcoming and inclusive environment for all!

{{< contributing-universities >}}

If you have any questions or need to reach out to the committee, send a note to [hedoc-committee-g@vt.edu](mailto:hedoc-committee-g@vt.edu).

## Accessibility and Inclusion

All pre-recorded material will be live-captioned and a recording will be made available following the event. If you are an individual with a disability and desire an accommodation, please contact our planning committee at [hedoc-committee-g@vt.edu](mailto:hedoc-committee-g@vt.edu) at least 10 business days prior to the event. We welcome everyone regardless of gender, sexual orientation, age, disability, physical appearance, body size, race, veteran status, religion, or immigration status.

We're following the Cloud Forum [code of conduct](https://blogs.cornell.edu/cloudforum/code-of-conduct/).

## Frequently Asked Questions

### What do you mean by "DevOps"?

Great question! If you ask five people to define DevOps, you might get ten answers! For the purposes of this conference, we are defining it as the following:

> DevOps encompasses all of the practices needed to quickly, safely, and reliably respond to the needs of our organizations and/or users when developing or delivering software.

This captures ideas around project management, application development, testing, deployment, cloud architecture, and more!


### Who is invited to attend?

If you are faculty or staff of an InCommon institution, you are welcome to attend _at no cost_!


### What content will be presented?

The goal of this conference is to help connect and share ideas and trainings about everything DevOps amongst ourselves. While we don't have the specific agenda yet (please [submit a proposal!!!](/cfp)), we hope to have content from all aspects of DevOps.


### Who will be presenting?

Great question! Hopefully... you! This conference provides the ability to share content amongst ourselves, and we want our speakers to reflect and represent that same community. The low-stress virtual presentation format offers a great opportunity for first-time speakers! If you have something to share, we want to help you do so! This can be a great opportunity to become a first-time speaker!

### Why is the conference taking place across three days in the afternoon?

We recognize that there are differences between in-person and virtual conferences. We know that most people attend virtual conferences while still having to perform their normal job functions. To help this, we decided to ensure you have half of your day to respond to email, respond to operational issues, or whatever else you have to do!

---
title: Agenda
layout: "single"
---

We're excited to have a great lineup of sessions for our inaugural conference! The first two days of our conference will consist of 30-minute sessions. The majority of the content is pre-recorded, which allows speakers to respond to questions in real-time in chat. Most speakers will have time at the end of the session for a live Q&A as well!

The third day will be less formal (dare we say... unconference-like?!?), encouraging networking and free-form discussion. More details will be coming soon about how it'll work!

Note that all times below are in EDT.

{{< session-list >}}

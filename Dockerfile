# Borrowed heavily from the jojomi/hugo container image
FROM klakegg/hugo:0.82.0-ext-alpine AS base

# A stage for dev
FROM base AS dev
CMD ["server", "--watch", "--source=/src", "--bind=0.0.0.0"]

# A stage for build
FROM base AS build
COPY . .
RUN hugo --source=/src --destination=/output

# Put final built content into an Nginx container
FROM nginx:alpine AS final
COPY --from=build /output /usr/share/nginx/html

# Higher-Ed DevOps Conference website

This repo contains the source material for the Higher-Ed DevOps Conference website, available at [https://hedoc.devcom.vt.edu](https://hedoc.devcom.vt.edu).

## Development

The site is generated using Hugo. To simplify development, a `docker-compose.yml` file is provided. You can start contributing by running the following:

```bash
docker compose up -d
```

Once the container is up and running, you can access the site at [http://localhost:1313](http://localhost:1313).
